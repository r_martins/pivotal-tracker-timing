﻿<?php
/**
Calcula tempo de cada historia do Pivotal Tracker
@author Ricardo Martins <ricardo@ricardomartins.info>
*/
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<h1>Calcular tempo das historias Pivotal Tracker</h1>
<p><a href="https://bitbucket.org/r_martins/pivotal-tracker-timing" target="_blank">Este projeto está no bitbucket</a></p>
<form method="GET" action="<?php echo $_SERVER['SCRIPT_NAME']?>">
Token: <input type="text" name="token" value="<?php echo isset($_GET['token'])?$_GET['token']:''?>"/>
Project ID: <input type="text" name="project" value="<?php echo isset($_GET['project'])?$_GET['project']:''?>"/>
<input type="submit"/>
</form>
<?php 
if(empty($_GET['project']) OR empty($_GET['token'])){
	exit;
}
$token = $_GET['token'];
$project = $_GET['project'];
$header = sprintf('X-TrackerToken: %s', $token);


$allstoriesUrl = "http://www.pivotaltracker.com/services/v3/projects/{$project}/stories?filter=current_state%3A%22accepted%22";

$ch = curl_init() or die('curl ext required');
curl_setopt($ch, CURLOPT_HTTPHEADER, array($header));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


curl_setopt($ch,CURLOPT_URL, $allstoriesUrl);
$xmlproject = simplexml_load_string(curl_exec($ch));

//var_dump($xmlproject);


$stories = array();
foreach($xmlproject->story as $project){
	$created = new DateTime($project->created_at);
	$accepted = new DateTime($project->accepted_at);
	//var_dump($project->created_at, $project->accepted_at);
	//var_dump($accepted->diff($created));
	$story['diff'] = $accepted->diff($created);
	$story['name'] = $project->name;
	$story['owner'] = $project->owned_by;
	$story['id'] = $project->id;
	$story['url'] = $project->url;
	$story['created_at'] = $created; //i need the START DATE!!!! grrr
	$story['accepted_at'] = $accepted;
	$stories[] = $story;
}

if(count($stories) == 0)
{
	echo "nao ha historias no projeto";
}else{
	echo '<p>' . count($stories) . ' historia(s)</p>';
	foreach($stories as $story){
		$created = $story['created_at']->format('d/m/Y à\s H:i:s');
		$accepted = $story['accepted_at']->format('d/m/Y à\s H:i:s');
		$diff = $story['diff']->format('%d dia(s), %h hora(s), %i minuto(s)');
		$html=<<<HTMLLOOP
			Story: <strong>{$story['name']}</strong> [<a href="{$story['url']}" target="_blank">{$story['id']}</a>]
			<br/>
			Dono: {$story['owner']}
			<br/>
			Criada em: {$created}
			<br/>
			Aceita em: {$accepted}
			<br/>
			Tempo: <strong>{$diff}</strong>
			<hr width="100%" size="1" noshade/>		
HTMLLOOP;

		echo $html;
	}
}
?>
</body>
</html>